#pragma once
#include "Polygon.h"

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	virtual ~Triangle();
	
	// override functions if need (virtual + pure virtual)
	void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);

	double getArea() const;
	double getPerimeter() const;
	void move(const Point& other);

private:
	Point _a;
	Point _b;
	Point _c;
};