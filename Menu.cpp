#include "Menu.h"


Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

void Menu::addCircle()
{
	double x = 0;
	double y = 0;
	double radius = 0;


	std::cout << "Please enter x:\n";
	std::cin >> x;
	std::cout << "Please enter y:\n";
	std::cin >> y;
	std::cout << "Please enter radius:\n";
	std::cin >> radius;
	
	_shapes.push_back(new Circle(Point(x, y), radius, "Circle", inputShapeName())); //adding to the _shapes vector the new circle
	_shapes[_shapes.size() - 1]->draw(*_disp, *_board);
}

void Menu::addArrow()
{
	double x1 = 0;
	double y1 = 0;
	double x2 = 0;
	double y2 = 0;

	std::cout << "Please enter x1:\n";
	std::cin >> x1;
	std::cout << "Please enter y1:\n";
	std::cin >> y1;
	std::cout << "Please enter x2:\n";
	std::cin >> x2;
	std::cout << "Please enter y2:\n";
	std::cin >> y2;

	_shapes.push_back(new Arrow(Point(x1, y1), Point(x2, y2), "Arrow", inputShapeName()));
	_shapes[_shapes.size() - 1]->draw(*_disp, *_board);
}

void Menu::addTriangle()
{
	double x1 = 0;
	double y1 = 0;
	double x2 = 0;
	double y2 = 0;
	double x3 = 0;
	double y3 = 0;

	std::cout << "Please enter x1:\n";
	std::cin >> x1;
	std::cout << "Please enter y1:\n";
	std::cin >> y1;
	std::cout << "Please enter x2:\n";
	std::cin >> x2;
	std::cout << "Please enter y2:\n";
	std::cin >> y2;
	std::cout << "Please enter x3:\n";
	std::cin >> x3;
	std::cout << "Please enter y3:\n";
	std::cin >> y3;

	if ((y1 == y2 && y2 == y3) || (x1 == x2 && x2 == x3))
	{
		std::cerr << "the points entered create a line\n";
		system("pause");
	}
	else
	{
		_shapes.push_back(new Triangle(Point(x1, y1), Point(x2, y2), Point(x3, y3), "Triangle", inputShapeName()));
		_shapes[_shapes.size() - 1]->draw(*_disp, *_board);
	}
	
}

void Menu::addRectangle()
{
	double x = 0;
	double y = 0;
	double width = 0;
	double length = 0;

	std::cout << "Please enter x:\n";
	std::cin >> x;
	std::cout << "Please enter y:\n";
	std::cin >> y;
	std::cout << "Please enter width:\n";
	std::cin >> width;
	std::cout << "Please enter length:\n";
	std::cin >> length;
	
	if (length <= 0 || width <= 0) //checking if valid
	{
		std::cerr << "Width and length has to be positive values\n";
		system("pause");
	}
	else

	{
		_shapes.push_back(new myShapes::Rectangle(Point(x, y), length, width, "Rectangle", inputShapeName()));
		_shapes[_shapes.size() - 1]->draw(*_disp, *_board);
	}

	
}

void Menu::printMainMenu()
{
	std::cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes\nEnter 3 to exit.\n";
}

void Menu::printShapeMenu()
{
	std::cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle.\n";
}

std::string Menu::inputShapeName()
{
	std::string result = "";

	std::cout << "Please enter the name of the shape:\n";
	std::cin >> result;

	return result;
}

void Menu::modifyShape()
{
	int i = 0;
	int shapeChoice = 0;
	int menuChoice = 0;

	//for move option
	int x = 0;
	int y = 0;

	do
	{//making sure shapeChoice is valid
		system("cls");
		for (i = 0; i < _shapes.size(); i++)
		{ //printing details of each shape
			std::cout << "Enter " << i << " for " << _shapes[i]->getName() << " (" << _shapes[i]->getType() << ")\n";
		}

		std::cin >> shapeChoice;

	} while (shapeChoice < 0 || shapeChoice >= _shapes.size());

	std::cout << "Enter 0 to move the shape\nEnter 1 to get its details.\nEnter 2 to remove the shape.\n";
	std::cin >> menuChoice;

	switch (menuChoice)
	{
	case MODIFY_MOVE:
		system("cls");
		std::cout << "Please enter the X moving scale: ";
		std::cin >> x;
		std::cout << "Please enter the Y moving scale: ";
		std::cin >> y;
		_shapes[shapeChoice]->clearDraw(*_disp, *_board);
		_shapes[shapeChoice]->move(Point(x, y));
		drawAll(); //redrawing all of the shapes including the moved one
		break;
	case MODIFY_DETAILS:
		_shapes[shapeChoice]->printDetails();
		system("pause"); //pausing the screen so the user will se the details (there's a 'cls' after this)
		break;

	case MODIFY_DELETE:
		_shapes[shapeChoice]->clearDraw(*_disp, *_board);
		_shapes.erase(_shapes.begin() + shapeChoice); //I have to actually remove the shape. if not, the shape will get re-draw at drawAll()
		drawAll(); //we have to draw everything again, because when clearing the drawing, the drawing is just turning black and not actually get deleted
		break;
	}
}

void Menu::drawAll()
{
	int i = 0;

	for (i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->draw(*_disp, *_board);
	}
}


void Menu::deleteShapes()
{
	int i = 0;

	for (i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->clearDraw(*_disp, *_board);
		_shapes.clear(); //if I will not clear this, the shapes will still exist! (and could be re-drawed)
	}
}

int Menu::shapeAmount()
{
	return _shapes.size();
}