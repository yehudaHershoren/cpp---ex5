#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>

#define SELECT_ADD_SHAPE 0
#define SELECT_MODIFY_SHAPE 1
#define SELECT_DELETE_ALL 2
#define SELECT_EXIT 3

#define ADD_CIRCLE 0
#define ADD_ARROW 1
#define ADD_TRIANGLE 2
#define ADD_RECTANGLE 3

#define MODIFY_MOVE 0
#define MODIFY_DETAILS 1
#define MODIFY_DELETE 2
 
class Menu
{
public:

	Menu();
	~Menu();

	void addCircle();
	void addArrow();
	void addTriangle();
	void addRectangle();

	void modifyShape();//handles with the option of modifying a shape
	void drawAll(); //draws all of the shapes in _shapes
	
	void deleteShapes();
	int shapeAmount(); //returns _shapes.size()

	void printMainMenu(); //printing the main menu text
	void printShapeMenu(); //printing the shape menu text


private: 
	std::string inputShapeName(); // inputting the name of the shape

	std::vector<Shape *> _shapes; //vector's cannot be an abstract class, so a pointer will fix it!
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

