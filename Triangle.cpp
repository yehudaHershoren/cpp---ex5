 #include "Triangle.h"
//#include <math.h>

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name):Polygon(name,type)
{	
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);

	_a = a;
	_b = b;
	_c = c;
}

Triangle::~Triangle()
{
}

double Triangle::getArea() const
{
	//by the shoelace formula
	return 0.5 * abs((_a.getX() - _c.getX()) * (_b.getY() - _a.getY()) - (_a.getX() - _b.getX()) * (_c.getY() - _a.getY()));
}

double Triangle::getPerimeter() const
{
	//side + side + side
	return (_a.distance(_b) + _b.distance(_c) + _c.distance(_a));
}

void Triangle::move(const Point& other)
{
	_a += other;
	_b += other;
	_c += other;
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
