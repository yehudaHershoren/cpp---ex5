#include "Shape.h"
#include <string>

Shape::Shape(const string& name, const string& type)
{
	_name = name;
	_type = type;
}


void Shape::printDetails() const
{
	std::cout << _type << "\t" << _name << "\t" << getArea() << "\t" << getPerimeter() << "\n";
}

string Shape::getType() const
{
	return _type;
}

string Shape::getName() const
{
	return _name;
}

void Shape::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{

}