#include "Point.h"
#include <math.h>
#define SQUERED 2

Point::Point()
{
	_x = 0;
	_y = 0;
}
Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}

Point::Point(const Point& other)
{
	_x = other.getX();
	_y = other.getY();
}

Point::~Point()
{

}

Point Point::operator+(const Point& other) const
{
	Point result = Point(other.getX() + _x, other.getY() + _y);
	return result;
}

Point& Point::operator+=(const Point& other)
{
	_x += other.getX();
	_y += other.getY();
	return *this;
}

double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

double Point::distance(const Point& other) const
{
	//according the formula: square root of (x+x)^2 + (y+y)^2
	return sqrt((pow((_x + other.getX()),SQUERED)  + pow((_y + other.getY()), SQUERED)));
}
