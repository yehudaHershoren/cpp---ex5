#include "Arrow.h"

Arrow::Arrow(const Point& a, const Point& b, const string& type, const string& name) : Shape(name, type)
{
	_p1 = a;
	_p2 = b;
}

Arrow::~Arrow()
{

}

void Arrow::move(const Point& other)
{
	_p1 += other;
	_p2 += other;
}

double Arrow::getPerimeter() const
{
	//the perimeter of an arrow is the distance between the two points
	return _p1.distance(_p2);
}

double Arrow::getArea() const
{
	//the area of an arrow is 0
	return NULL;
}


void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}


