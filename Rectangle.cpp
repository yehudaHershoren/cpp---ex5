#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name):Polygon(name, type)
{
	_a = a;
	_length = length;
	_width = width;
	_points.push_back(a);
	_points.push_back(Point(a.getX() + length, a.getY() + width)); //creating and pushing to the vector the right bottom point of the rectagnle 

}

myShapes::Rectangle::~Rectangle()
{

}

void myShapes::Rectangle::move(const Point& other)
{
	_a += other;
}

double myShapes::Rectangle::getArea() const
{
	return _width * _length;
}

double myShapes::Rectangle::getPerimeter() const
{
	return 2 * _width + 2 * _length;
}

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


